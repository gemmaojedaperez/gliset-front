import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignerLoginComponent } from './designer-login.component';

describe('DesignerLoginComponent', () => {
  let component: DesignerLoginComponent;
  let fixture: ComponentFixture<DesignerLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignerLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
