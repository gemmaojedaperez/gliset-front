import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DesignerRoutingModule } from './designer-routing.module';
import { DesignerBackgroundComponent } from './components/designer-background/designer-background.component';
import { DesignerLoginComponent } from './container/designer-login/designer-login.component';

@NgModule({
  declarations: [
    DesignerBackgroundComponent,
    DesignerLoginComponent
  ],
  imports: [
    CommonModule,
    DesignerRoutingModule,
   
  ]
})
export class DesignerModule { }
