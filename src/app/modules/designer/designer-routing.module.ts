import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesignerLoginComponent } from './container/designer-login/designer-login.component';

const routes: Routes = [
  {
    path: 'login',
    component: DesignerLoginComponent
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignerRoutingModule { }
