import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignerBackgroundComponent } from './designer-background.component';

describe('DesignerBackgroundComponent', () => {
  let component: DesignerBackgroundComponent;
  let fixture: ComponentFixture<DesignerBackgroundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignerBackgroundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignerBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
