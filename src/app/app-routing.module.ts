import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "designer", 
    loadChildren: () => import('./modules/designer/designer.module').then(m => m.DesignerModule)
  },
  {
    path: '**',
    redirectTo: 'designer'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
